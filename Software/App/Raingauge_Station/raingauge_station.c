/*
 * raingauge_station.c
 *
 *  Created on: 16-May-2022
 *      Author: arun
 */

#include <string.h>
#include <stdlib.h>
#include "hw.h"

#include "raingauge_station.h"


#define R1 	10		/*10 K resistor R1 in voltage divider*/
#define R2 	10		/*10 K resistor R2 in voltage divider*/

#define RAINGAUGE_PORT			GPIOB
#define RAINGAUGE_PIN			GPIO_PIN_14

#define BATT_ENABLE_PORT		GPIOB
#define BATT_ENABLE_PIN			GPIO_PIN_2
#define BATTERY_CHANNEL			ADC_CHANNEL_4

#define UV_ENABLE_PORT	        GPIOB
#define UV_ENABLE_PIN		    GPIO_PIN_13
#define UV_CHANNEL		        ADC_CHANNEL_3


static void writeToMemory(uint32_t Address, uint32_t data);
static uint16_t readFromMemory(uint32_t Address);

static uint16_t readAnalogUvValue(void);
static uint32_t map(uint32_t au32_IN, uint32_t au32_INmin, uint32_t au32_INmax, uint32_t au32_OUTmin, uint32_t au32_OUTmax);

uint16_t rainGaugetipCount = 0; /*   rainCount increment for every interrupt rain fall */
uint8_t dailyCountFlag = RESET;
uint16_t TotalAccumulatedRainfall=0;

/*  readBatteryLevel   */
/**
 * Created on: May 16, 2022
 * Last Edited: May 16, 2022
 * Author: Arun-- ICFOSS
 *
 * @brief  read external battery voltage connected to analog channel 4 through a voltage divider
 * @param  none
 * @retval battery voltage level
 *
 **/
uint16_t readBatteryLevel(void) {
	int analogValue = 0; /*   adc reading for battery is stored in the variable  */
	float batteryVoltage = 0;
	uint16_t batteryLevel = 0; /*    battery voltage   */

	/* enable battery voltage reading */
	enable(BATT_POWER);
	HAL_Delay(10);

	/* Read battery voltage reading */
	analogValue = HW_AdcReadChannel(BATTERY_CHANNEL);

	/* disable battery voltage reading */
	disable(BATT_POWER);

	/*battery voltage = ADC value*Vref*2/4096   --12 bit ADC with voltage divider factor of 2 */
	batteryVoltage = (analogValue * 3.3 * ((R1 + R2) / R2)) / 4096;

	/*multiplication factor of 100 to convert to int from float*/
	batteryLevel = (uint16_t) (batteryVoltage * 100);

	return batteryLevel;
}

/*  rainGaugeInterruptEnable  */
/**
 * Created on: May 16, 2022
 * Last Edited: May 16, 2022
 * Author: Arun
 *
 * @brief initialise a gpio pin in interrupt mode to read every falling edge
 * @note this pin PA0 is connected to a Rain gauge Reed sensor output
 * @param none
 * @retval none
 *
 **/
void rainGaugeInterruptEnable() {

	GPIO_InitTypeDef initStruct = { 0 };

	initStruct.Mode = GPIO_MODE_IT_FALLING;
	initStruct.Pull = GPIO_PULLUP;
	initStruct.Speed = GPIO_SPEED_HIGH;

	HW_GPIO_Init(RAINGAUGE_PORT, RAINGAUGE_PIN, &initStruct);
	HW_GPIO_SetIrq(RAINGAUGE_PORT, RAINGAUGE_PIN, 0, rainGaugeTips);
}

/*  rainGaugeTips  */
/**
 * Created on: May 16, 2022
 * Last Edited: May 16, 2022
 * Author: Arun
 *
 * @brief callback function interrupt on PA9 pin
 * @note this pin PA0 is connected to a Rain gauge Reed sensor output
 * @param none
 * @retval none
 *
 **/
void rainGaugeTips() {
	rainGaugetipCount++;
}

/*  getAccumulatedRainfall  */
/**
 * Created on: May 16, 2022
 * Last Edited: May 16, 2022
 * Author: Arun
 *
 * @brief get total accumulated rainfall at transmission interval
 * @note after transmission of rainfall the data is reset and starts new count
 * @param none
 * @retval total rainfall at specified interval (sending only the number of tips, it has to be multiplied by the multiplication factor at the decoding end)
 *
 **/
uint16_t getAccumulatedRainfall() {
	uint16_t rainfall = 0;
	rainfall = rainGaugetipCount;
	TotalAccumulatedRainfall += rainfall;
	rainGaugetipCount = 0;
	return (uint16_t) rainfall;

}

/*  getTotalRainfall  */
/**
 * Created on: Jul 13, 2022
 * Last Edited: Jul 13, 2022
 * Author: Ajmi
 *
 * @brief calculate accumulated rainfall over a period of time (eg: here we get one day data configured using a downlink from server every morning at 8am)
 * @note if downlink received totalrainfall data is reset and starts new count // also writes to memory location
 * @param none
 * @retval totalrainfall at specified interval (sending only the number of tips, it has to be multiplied by the multiplication factor at the decoding end)
 *
 **/
uint16_t getTotalRainfall(uint8_t downlinkReceived){
	uint16_t TotalRainfall=0;

	if(downlinkReceived == RESET){

		TotalRainfall = TotalAccumulatedRainfall;

	}
	else{
		//TotalRainfall=0;
		TotalAccumulatedRainfall=0;
		dailyCountFlag = RESET;
	}
	writeToMemory(RAIN_MEMORY_ADD, (uint16_t)TotalRainfall);
	return (uint16_t) TotalRainfall;
}

/*uint16_t getdownlinkRainfall(){
	uint16_t dailRainfall=0;
	dailRainfall = getTotalRainfall();
	dailyCountFlag = LORA_SET;
	return (uint16_t)dailRainfall;

}*/


void raingaugeStationGPIO_Init() {
	GPIO_InitTypeDef initStruct = { 0 };
	initStruct.Pull = GPIO_NOPULL;
	initStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	initStruct.Mode = GPIO_MODE_OUTPUT_PP;

	HW_GPIO_Init(BATT_ENABLE_PORT, BATT_ENABLE_PIN, &initStruct);
	HW_GPIO_Init(UV_ENABLE_PORT, UV_ENABLE_PIN, &initStruct);

}

/*  enable  */
/**
 * Created on: May 16, 2022
 * Last Edited: May 16, 2022
 * Author: Arun
 *
 * @brief manual control of gpio
 * @param the gpio to be enabled
 * @retval none
 *
 **/
void enable(uint8_t pin) {

	switch (pin) {
		case 1:
			HAL_GPIO_WritePin(BATT_ENABLE_PORT, BATT_ENABLE_PIN, GPIO_PIN_RESET); //for battery
			break;
		case 2:
			HAL_GPIO_WritePin(UV_ENABLE_PORT, UV_ENABLE_PIN, GPIO_PIN_SET);
			break;
}
}

/*  disable  */
/**
 * Created on: May 16, 2022
 * Last Edited: May 16, 2022
 * Author: Arun
 *
 * @brief manual control of gpio
 * @param the gpio to be disabled
 * @retval none
 *
 **/
void disable(uint8_t pin) {

	switch (pin) {
		case 1:
			HAL_GPIO_WritePin(BATT_ENABLE_PORT, BATT_ENABLE_PIN, GPIO_PIN_SET); //for battery
			break;
		case 2:
			HAL_GPIO_WritePin(UV_ENABLE_PORT, UV_ENABLE_PIN, GPIO_PIN_RESET);
			break;

}
}
/*  raingaugeStationInit */
/**
 * Created on: May 16, 2022
 * Last Edited: May 16, 2022
 * Author: Arun
 *
 * @brief initialize  raingauge station system
 * @param none
 * @retval none
 *
 **/

void raingaugeStationInit() {

	rainGaugeInterruptEnable();
	raingaugeStationGPIO_Init();
	TotalAccumulatedRainfall = readFromMemory(RAIN_MEMORY_ADD);
}

/*  readWeatherStationParameters*/
/**
 * Created on: May 16, 2022
 * Last Edited: May 16, 2022
 * Author: Arun
 *
 * @brief initialize  weather station system
 * @param none
 * @retval none
 *
 **/
void readRaingaugeStationParameters(rainfallData_t *sensor_data) {

	sensor_data->batteryLevel = readBatteryLevel();
	sensor_data->rainfall = getAccumulatedRainfall(); // 15 minutes total
//	sensor_data->totalrainfall = getTotalRainfall();  // 1 day total
	sensor_data->uv = readAnalogUvValue();

}

/*  readRainAccumulation*/
/**
 * Created on: Jun 1, 2022
 * Last Edited: Jun 1, 2022
 * Author: Arun
 *
 * @brief read and reset accumulated Rainfall data
 * @param none
 * @retval none
 *
 **/
/*void readRainAccumulation(rainfallData_t *sensor_data) {

	sensor_data->downlinkRainfall = getdownlinkRainfall();

}*/

/*  readAnalogUvValue   */
/**
 * Created on: Nov 21, 2022
 * Last Edited: Nov 21, 2022
 * Author: Sruthy-- ICFOSS
 *
 * @brief  read pressure value connected to analog channel 0
 * @param  none
 * @retval battery voltage level
 *
 **/
static uint16_t readAnalogUvValue(void) {
	uint16_t analogValue = 0; /*   adc reading for battery is stored in the variable  */
	uint16_t uv = 0;


	/* enable analog reading */
//	enable(PRESSURE_POWER);

	/* Read battery voltage reading */
	analogValue = HW_AdcReadChannel(UV_CHANNEL);
	PRINTF("uvvalue = %d \r\n", analogValue);
	/*map analog values to pressure values */
	uv = map(analogValue,0,3078,0,16);
	PRINTF("uvindex = %d \r\n", uv);
	/* disable analog reading */
	//disable(PRESSURE_POWER);

	return uv;
}

/*  map   */
/**
 * Created on: Jun 21, 2022
 * Last Edited: Jun 21, 2022
 * Author: Ajmi-- ICFOSS
 *
 * @brief  Re-maps a number from one range to another
 * @param  value to be mapped, actual range min, actual range max, new range min, new range max
 * @retval mapped value
 *
 **/
static uint32_t map(uint32_t au32_IN, uint32_t au32_INmin, uint32_t au32_INmax, uint32_t au32_OUTmin, uint32_t au32_OUTmax){
	return ((((au32_IN - au32_INmin)*(au32_OUTmax - au32_OUTmin))/(au32_INmax - au32_INmin)) + au32_OUTmin);
}



static void writeToMemory(uint32_t Address, uint32_t data)
{
	/*EEPROM DATA STORING*/

	HAL_FLASHEx_DATAEEPROM_Unlock();
	HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, Address, data);
	HAL_FLASHEx_DATAEEPROM_Lock();

}

static uint16_t readFromMemory(uint32_t Address){
	return *(uint32_t *)Address;
}
