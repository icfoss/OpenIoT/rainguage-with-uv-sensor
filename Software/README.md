Automatic Rain Gauge Station

LoRaWAN based Automatic Rain Gauge Station is a system which measure the Total rainfall and transmits the data at regular intervals.

List of Parameters recorded

- Every 15 minutes rainfall data
- Daily Rainfall data(Transmits every day 8.00 AM)


LoRaWAN transmission is every 15 minutes.

The application code is written on top of the I-cube-lrwan package by STM32. 
